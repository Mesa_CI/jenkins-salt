/etc/kernel/postinst.d/:
    file.directory:
        - user: root
        - group: root
        - mode: 755

/etc/kernel/postrm.d/:
    file.directory:
        - user: root
        - group: root
        - mode: 755

/etc/kernel/postinst.d/zzz-install-grub-cfg-to-efi:
    file.managed:
        - source: salt://slave/files/kernel/postinst.d/zzz-install-grub-cfg-to-efi
        - user: root
        - group: root
        - mode: 755

/etc/kernel/postrm.d/zzz-install-grub-cfg-to-efi:
    file.managed:
        - source: salt://slave/files/kernel/postrm.d/zzz-install-grub-cfg-to-efi
        - user: root
        - group: root
        - mode: 755
