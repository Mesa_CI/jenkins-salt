machine_name=$(cut -d '-' -f3 <<<"$(hostname)")-$(cut -d '-' -f4 <<<"$(hostname)")

[[ $(curl -s "http://192.168.1.1/computer/$machine_name/api/json?pretty=true" |grep "\"offline\".*:.*false") ]] && \
        echo  &&\
        echo "**********************************************************************" && \
        echo "*  WARNING: This system is ONLINE in Jenkins and will accept CI jobs *" && \
        echo "**********************************************************************"
