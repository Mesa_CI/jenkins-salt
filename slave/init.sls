include:
    - slave.dhcp
    - slave.docker
    - slave.packages
    - slave.ssh_keys
    - slave.sysctl
    - slave.systemd
    - slave.jenkins_online_check
    - slave.kernel
