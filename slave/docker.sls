/etc/docker/:
    file.directory:
        - user: root
        - group: root
        - mode: 755

/etc/docker/daemon.json:
    file.managed:
        - source: salt://slave/files/docker/daemon.json
        - user: root
        - group: root
        - mode: 644
