/etc/security/limits.d/10-enable-core-dumps.conf:
  file.managed:
    - source: salt://runner/files/10-enable-core-dumps.conf
    - user: root
    - group: root
    - mode: 644
# vim: ft=yaml
