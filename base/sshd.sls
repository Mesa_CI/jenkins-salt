/etc/ssh/sshd_config.d:
    file.directory:
        - user: root
        - group: root
        - mode: 755

/etc/ssh/sshd_config.d/10-dh-sha1.conf:
    file.managed:
        - source: salt://base/files/sshd_config/10-dh-sha1.conf
        - user: root
        - group: root
        - mode: 644
        - require:
              - file: /etc/ssh/sshd_config.d
