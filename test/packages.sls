# The format used by runner-packages allows for a very terse list of all pacakges
# for more complex requirements see:
# http://docs.saltstack.com/en/latest/ref/states/all/salt.states.pkg.html

include:
    - base.timesyncd
    - base.sources
    - slave.apt

latest-libc-packages:
    # keep these packages up to date to prevent failures when builders get libc updates
    pkg.latest:
        - require:
            - sls: base.timesyncd
            - sls: base.sources
            - sls: slave.apt
        - refresh: true
        - normalize: false
        - pkgs:
            - libc6
            - libc6-dev
            - libc6-dev:i386
            - libc6:i386

latest-mesa-packages:
    # keep these packages up to date to prevent X-server crashes
    pkg.latest:
        - require:
            - sls: base.timesyncd
            - sls: base.sources
            - sls: slave.apt
        - refresh: True
        - normalize: False
        - pkgs:
            - libd3dadapter9-mesa
            - libd3dadapter9-mesa-dev
            - libd3dadapter9-mesa-dev:i386
            - libd3dadapter9-mesa:i386
            - libegl-mesa0
            - libegl-mesa0:i386
            - libegl1-mesa
            - libegl1-mesa-dev
            - libegl1-mesa-dev:i386
            - libegl1-mesa:i386
            - libgbm-dev
            - libgbm-dev:i386
            - libgbm1
            - libgbm1:i386
            - libgl1-mesa-dev
            - libgl1-mesa-dev:i386
            - libgl1-mesa-dri
            - libgl1-mesa-dri:i386
            - libgl1-mesa-glx
            - libgl1-mesa-glx:i386
            - libglapi-mesa
            - libglapi-mesa:i386
            - libgles2-mesa
            - libgles2-mesa-dev
            - libgles2-mesa-dev:i386
            - libgles2-mesa:i386
            - libglx-mesa0
            - libglx-mesa0:i386
            - libosmesa6
            - libosmesa6-dev
            - libosmesa6-dev:i386
            - libosmesa6:i386
            - libwayland-egl1-mesa
            - libwayland-egl1-mesa:i386
            - libxatracker-dev
            - libxatracker-dev:i386
            - libxatracker2
            - libxatracker2:i386
            - mesa-common-dev
            - mesa-common-dev:i386
            - mesa-opencl-icd
            - mesa-opencl-icd:i386
            - mesa-va-drivers
            - mesa-va-drivers:i386
            - mesa-vdpau-drivers
            - mesa-vdpau-drivers:i386
            - mesa-vulkan-drivers
            - mesa-vulkan-drivers:i386
            - xserver-xorg-core

# vim: ft=yaml
