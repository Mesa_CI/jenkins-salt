---
/etc/systemd/network/bond0.network:
    file.managed:
        - source: salt://master/files/networkd/bond0.network
        - user: root
        - group: root
        - mode: 644

/etc/systemd/network/bond0.netdev:
    file.managed:
        - source: salt://master/files/networkd/bond0.netdev
        - user: root
        - group: root
        - mode: 644

/etc/systemd/network/eth1.network:
    file.managed:
        - source: salt://master/files/networkd/eth1.network
        - user: root
        - group: root
        - mode: 644

/etc/systemd/network/team.network:
    file.managed:
        - source: salt://master/files/networkd/team.network
        - user: root
        - group: root
        - mode: 644
