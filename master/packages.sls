# The format used by runner-packages allows for a very terse list of all pacakges
# for more complex requirements see:
# http://docs.saltstack.com/en/latest/ref/states/all/salt.states.pkg.html

master-packages:
    pkg.installed:
        - refresh: True
        - normalize: False
        - pkgs:
            - tsocks
            - screen
            - openjdk-8-jre
            - python3-bs4
            - python3-git
            - vim
            - vim-nox
            # needed for mesa_perf_history service
            - python3-pony
            - python3-pymysql

# vim: ft=yaml
